# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

from AthenaCommon.CfgGetter import addAlgorithm

addAlgorithm("OverlayCopyAlgs.OverlayCopyAlgsConfigLegacy.getCopyCaloCalibrationHitContainer", "CopyCaloCalibrationHitContainer")
addAlgorithm("OverlayCopyAlgs.OverlayCopyAlgsConfigLegacy.getCopyInTimeAntiKt4JetTruthInfo", "CopyInTimeAntiKt4JetTruthInfo")
addAlgorithm("OverlayCopyAlgs.OverlayCopyAlgsConfigLegacy.getCopyOutOfTimeAntiKt4JetTruthInfo", "CopyOutOfTimeAntiKt4JetTruthInfo")
addAlgorithm("OverlayCopyAlgs.OverlayCopyAlgsConfigLegacy.getCopyInTimeAntiKt6JetTruthInfo", "CopyInTimeAntiKt6JetTruthInfo")
addAlgorithm("OverlayCopyAlgs.OverlayCopyAlgsConfigLegacy.getCopyOutOfTimeAntiKt6JetTruthInfo", "CopyOutOfTimeAntiKt6JetTruthInfo")
addAlgorithm("OverlayCopyAlgs.OverlayCopyAlgsConfigLegacy.getCopyPileupParticleTruthInfo", "CopyPileupParticleTruthInfo")
addAlgorithm("OverlayCopyAlgs.OverlayCopyAlgsConfigLegacy.getCopyMcEventCollection", "CopyMcEventCollection")
addAlgorithm("OverlayCopyAlgs.OverlayCopyAlgsConfigLegacy.getCopyTimings", "CopyTimings")
addAlgorithm("OverlayCopyAlgs.OverlayCopyAlgsConfigLegacy.getCopyTrackRecordCollection", "CopyTrackRecordCollection")
